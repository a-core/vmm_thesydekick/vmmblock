module comp_amp 	(
		 input clk,
		 input reset,
		 input rdb_wr,
		 input local_wr,
		 input en,
		 input lsb_tune,
		 input  [5:0] config_count,
		 output [5:0] count_out,	 
		 input ana_in
		);
	reg  [5:0] counter_val;
	reg [5 : 0] count;
	
always @(posedge(clk)) begin 
				if (reset == 0) begin
				 count <= 6'b000000;
				 counter_val <= 6'b000000;
				end

				else if (rdb_wr==1 && local_wr==1) begin
	   			 counter_val<=config_count;	
				end			 

				else if (ana_in == 1 && en==1 && local_wr==0) begin
				 count <= count + 6'b000000;
	   			 counter_val<=counter_val + 6'b000000;	
				end

				else if (ana_in == 0 && en==1 && local_wr==0) begin
				 count <= count + 6'b000001;
	   			 counter_val<= counter_val + 6'b000001;	
				end
				
			 		 
		      end

assign count_out= local_wr ? counter_val-lsb_tune : count-lsb_tune;


endmodule
