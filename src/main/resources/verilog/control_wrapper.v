module control_wrapper(


    input core_clk,     
    input adc_clk,
    input Vref1_ADC,
    input Vref2_ADC,
    input Ibias_12uA,
    input mclk,
    input Vref_l_comp,
    input Vref_h_comp,
    input Vref_l_in,
    input Vref_h_in,
    input VCM_0p4V,

//   0x00
//1. [XXX adc_mux_config<4:0>(5'b0 0000)] 
//2. [XXX r_tune(1'b1) , comp_en(1'b0), comp_rdb_wr_gbl(1'b0), lsb_tune(1'b0), r3_tune<8>(1'b1)] 
//3. [r3_tune<7:0>(8'b0000 0000)] 
//4. [X, reset_ip_comp(1'b0), reset_op_comp(1'b0), reset_sram(1'b0), reset_adc(1'b0), sh_en(1'b0), vcm_en(1'b0), adc_mux_wr(1'b0)]
input [31:0] vmm_reset_control, 
// 
//   0x04
//1. [XXXX XXXX] 
//2. [sram_din<7:0>(8'b0000 0000)] 
//3. [XX sram_rd(1'b0), sram_wr(1'b0)   sram_addr<11:8>(4'b0000)] 
//4. [sram_addr<7:0>(8'b0000 0000)] 
input [31:0] sram_din_control_addr,

//   0x08
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XX hold_data_ready(1'b0) ,adc_data_ready(1'b0)] 
//4. [sram_dout<7:0>(8'b0000 0000)] 

output [31:0] vmm_ready_sram_dout,

//   0x12 - 0x140
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XXXX] 
//4. [XX adc_**<5:0>(6'b00 0000)] 	
output  [31:0] adc_0, 	
output  [31:0] adc_1,	
output  [31:0] adc_2,
output  [31:0] adc_3,
output  [31:0] adc_4,
output  [31:0] adc_5,
output  [31:0] adc_6,
output  [31:0] adc_7,
output  [31:0] adc_8,
output  [31:0] adc_9,
output  [31:0] adc_10,
output  [31:0] adc_11,
output  [31:0] adc_12,
output  [31:0] adc_13,
output  [31:0] adc_14,
output  [31:0] adc_15,
output  [31:0] adc_16,
output  [31:0] adc_17,
output  [31:0] adc_18,
output  [31:0] adc_19,
output  [31:0] adc_20,
output  [31:0] adc_21,
output  [31:0] adc_22,
output  [31:0] adc_23,
output  [31:0] adc_24,
output  [31:0] adc_25,
output  [31:0] adc_26,
output  [31:0] adc_27,
output  [31:0] adc_28,
output  [31:0] adc_29,
output  [31:0] adc_30,
output  [31:0] adc_31,

//   0x144
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XXXX] 
//4. [X comp_rdb_wr_local(1'b0), count_config_in_0<5:0>(6'b00 0000)] 
input  [31:0] count_config_in_0,
//   0x148
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XXXX] 
//4. [XX monitor_config_in_0<5:0>(6'b00 0000)] 
output [31:0] monitor_config_in_0,

 //    0x152
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XXXX] 
//4. [X comp_rdb_wr_local(1'b0), count_config_in_1<5:0>(6'b00 0000)] 
input  [31:0] count_config_in_1, 

 //    0x156
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XXXX] 
//4. [XX monitor_config_in_1<5:0>(6'b00 0000)] 
output  [31:0] monitor_config_in_1, 

 //    0x160
input [31:0] count_config_in_2, 

 //    0x164
output  [31:0] monitor_config_in_2, 

 //    0x168
input [31:0] count_config_in_3, 

 //    0x172
output  [31:0] monitor_config_in_3, 

 //    0x176
input [31:0] count_config_in_4, 

 //    0x180
output  [31:0] monitor_config_in_4, 

 //    0x184
input [31:0] count_config_in_5, 

 //    0x188
output  [31:0] monitor_config_in_5, 

 //    0x192
input [31:0] count_config_in_6, 

 //    0x196
output  [31:0] monitor_config_in_6, 

 //    0x200
input [31:0] count_config_in_7, 

 //    0x204
output  [31:0] monitor_config_in_7, 

 //    0x208
input [31:0] count_config_in_8, 

 //    0x212
output  [31:0] monitor_config_in_8, 

 //    0x216
input [31:0] count_config_in_9, 

 //    0x220
output  [31:0] monitor_config_in_9, 

 //    0x224
input [31:0] count_config_in_10, 

 //    0x228
output  [31:0] monitor_config_in_10, 

 //    0x232
input [31:0] count_config_in_11, 

 //    0x236
output  [31:0] monitor_config_in_11, 

 //    0x240
input [31:0] count_config_in_12, 

 //    0x244
output  [31:0] monitor_config_in_12, 

 //    0x248
input [31:0] count_config_in_13, 

 //    0x252
output  [31:0] monitor_config_in_13, 

 //    0x256
input [31:0] count_config_in_14, 

 //    0x260
output  [31:0] monitor_config_in_14, 

 //    0x264
input [31:0] count_config_in_15, 

 //    0x268
output  [31:0] monitor_config_in_15, 

 //    0x272
input [31:0] count_config_in_16, 

 //    0x276
output  [31:0] monitor_config_in_16, 

 //    0x280
input [31:0] count_config_in_17, 

 //    0x284
output  [31:0] monitor_config_in_17, 

 //    0x288
input [31:0] count_config_in_18, 

 //    0x292
output  [31:0] monitor_config_in_18, 

 //    0x296
input [31:0] count_config_in_19, 

 //    0x300
output  [31:0] monitor_config_in_19, 

 //    0x304
input [31:0] count_config_in_20, 

 //    0x308
output  [31:0] monitor_config_in_20, 

 //    0x312
input [31:0] count_config_in_21, 

 //    0x316
output  [31:0] monitor_config_in_21, 

 //    0x320
input [31:0] count_config_in_22, 

 //    0x324
output  [31:0] monitor_config_in_22, 

 //    0x328
input [31:0] count_config_in_23, 

 //    0x332
output  [31:0] monitor_config_in_23, 

 //    0x336
input [31:0] count_config_in_24, 

 //    0x340
output  [31:0] monitor_config_in_24, 

 //    0x344
input [31:0] count_config_in_25, 

 //    0x348
output  [31:0] monitor_config_in_25, 

 //    0x352
input [31:0] count_config_in_26, 

 //    0x356
output  [31:0] monitor_config_in_26, 

 //    0x360
input [31:0] count_config_in_27, 

 //    0x364
output  [31:0] monitor_config_in_27, 

 //    0x368
input [31:0] count_config_in_28, 

 //    0x372
output  [31:0] monitor_config_in_28, 

 //    0x376
input [31:0] count_config_in_29, 

 //    0x380
output  [31:0] monitor_config_in_29, 

 //    0x384
input [31:0] count_config_in_30, 

 //    0x388
output  [31:0] monitor_config_in_30, 

 //    0x392
input [31:0] count_config_in_31, 

 //    0x396
output  [31:0] monitor_config_in_31, 

 //    0x400
input [31:0] count_config_in_32, 

 //    0x404
output  [31:0] monitor_config_in_32, 

 //    0x408
input [31:0] count_config_in_33, 

 //    0x412
output  [31:0] monitor_config_in_33, 

 //    0x416
input [31:0] count_config_in_34, 

 //    0x420
output  [31:0] monitor_config_in_34, 

 //    0x424
input [31:0] count_config_in_35, 

 //    0x428
output  [31:0] monitor_config_in_35, 

//////////////////////////////////////
//SIMILARLY FOR  STAGE 64-AMPS >>>>>>>>>>>>>>>>>>>
/////////////////////////////////////

//    0x432
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XXXX] 
//4. [X comp_rdb_wr_local(1'b0), count_config_op_0<5:0>(6'b00 0000)] 
input [31:0] count_config_op_0, 

 //    0x436
//1. [XXXX XXXX] 
//2. [XXXX XXXX] 
//3. [XXXX XXXX] 
//4. [XX monitor_config_op_0<5:0>(6'b00 0000)] 
output [31:0] monitor_config_op_0, 

 //    0x440
input [31:0] count_config_op_1, 

 //    0x444
output [31:0] monitor_config_op_1, 

 //    0x448
input [31:0] count_config_op_2, 

 //    0x452
output [31:0] monitor_config_op_2, 

 //    0x456
input [31:0] count_config_op_3, 

 //    0x460
output [31:0] monitor_config_op_3, 

 //    0x464
input [31:0] count_config_op_4, 

 //    0x468
output [31:0] monitor_config_op_4, 

 //    0x472
input [31:0] count_config_op_5, 

 //    0x476
output [31:0] monitor_config_op_5, 

 //    0x480
input [31:0] count_config_op_6, 

 //    0x484
output [31:0] monitor_config_op_6, 

 //    0x488
input [31:0] count_config_op_7, 

 //    0x492
output [31:0] monitor_config_op_7, 

 //    0x496
input [31:0] count_config_op_8, 

 //    0x500
output [31:0] monitor_config_op_8, 

 //    0x504
input [31:0] count_config_op_9, 

 //    0x508
output [31:0] monitor_config_op_9, 

 //    0x512
input [31:0] count_config_op_10, 

 //    0x516
output [31:0] monitor_config_op_10, 

 //    0x520
input [31:0] count_config_op_11, 

 //    0x524
output [31:0] monitor_config_op_11, 

 //    0x528
input [31:0] count_config_op_12, 

 //    0x532
output [31:0] monitor_config_op_12, 

 //    0x536
input [31:0] count_config_op_13, 

 //    0x540
output [31:0] monitor_config_op_13, 

 //    0x544
input [31:0] count_config_op_14, 

 //    0x548
output [31:0] monitor_config_op_14, 

 //    0x552
input [31:0] count_config_op_15, 

 //    0x556
output [31:0] monitor_config_op_15, 

 //    0x560
input [31:0] count_config_op_16, 

 //    0x564
output [31:0] monitor_config_op_16, 

 //    0x568
input [31:0] count_config_op_17, 

 //    0x572
output [31:0] monitor_config_op_17, 

 //    0x576
input [31:0] count_config_op_18, 

 //    0x580
output [31:0] monitor_config_op_18, 

 //    0x584
input [31:0] count_config_op_19, 

 //    0x588
output [31:0] monitor_config_op_19, 

 //    0x592
input [31:0] count_config_op_20, 

 //    0x596
output [31:0] monitor_config_op_20, 

 //    0x600
input [31:0] count_config_op_21, 

 //    0x604
output [31:0] monitor_config_op_21, 

 //    0x608
input [31:0] count_config_op_22, 

 //    0x612
output [31:0] monitor_config_op_22, 

 //    0x616
input [31:0] count_config_op_23, 

 //    0x620
output [31:0] monitor_config_op_23, 

 //    0x624
input [31:0] count_config_op_24, 

 //    0x628
output [31:0] monitor_config_op_24, 

 //    0x632
input [31:0] count_config_op_25, 

 //    0x636
output [31:0] monitor_config_op_25, 

 //    0x640
input [31:0] count_config_op_26, 

 //    0x644
output [31:0] monitor_config_op_26, 

 //    0x648
input [31:0] count_config_op_27, 

 //    0x652
output [31:0] monitor_config_op_27, 

 //    0x656
input [31:0] count_config_op_28, 

 //    0x660
output [31:0] monitor_config_op_28, 

 //    0x664
input [31:0] count_config_op_29, 

 //    0x668
output [31:0] monitor_config_op_29, 

 //    0x672
input [31:0] count_config_op_30, 

 //    0x676
output [31:0] monitor_config_op_30, 

 //    0x680
input [31:0] count_config_op_31, 

 //    0x684
output [31:0] monitor_config_op_31, 

 //    0x688
input [31:0] count_config_op_32, 

 //    0x692
output [31:0] monitor_config_op_32, 

 //    0x696
input [31:0] count_config_op_33, 

 //    0x700
output [31:0] monitor_config_op_33, 

 //    0x704
input [31:0] count_config_op_34, 

 //    0x708
output [31:0] monitor_config_op_34, 

 //    0x712
input [31:0] count_config_op_35, 

 //    0x716
output [31:0] monitor_config_op_35, 

 //    0x720
input [31:0] count_config_op_36, 

 //    0x724
output [31:0] monitor_config_op_36, 

 //    0x728
input [31:0] count_config_op_37, 

 //    0x732
output [31:0] monitor_config_op_37, 

 //    0x736
input [31:0] count_config_op_38, 

 //    0x740
output [31:0] monitor_config_op_38, 

 //    0x744
input [31:0] count_config_op_39, 

 //    0x748
output [31:0] monitor_config_op_39, 

 //    0x752
input [31:0] count_config_op_40, 

 //    0x756
output [31:0] monitor_config_op_40, 

 //    0x760
input [31:0] count_config_op_41, 

 //    0x764
output [31:0] monitor_config_op_41, 

 //    0x768
input [31:0] count_config_op_42, 

 //    0x772
output [31:0] monitor_config_op_42, 

 //    0x776
input [31:0] count_config_op_43, 

 //    0x780
output [31:0] monitor_config_op_43, 

 //    0x784
input [31:0] count_config_op_44, 

 //    0x788
output [31:0] monitor_config_op_44, 

 //    0x792
input [31:0] count_config_op_45, 

 //    0x796
output [31:0] monitor_config_op_45, 

 //    0x800
input [31:0] count_config_op_46, 

 //    0x804
output [31:0] monitor_config_op_46, 

 //    0x808
input [31:0] count_config_op_47, 

 //    0x812
output [31:0] monitor_config_op_47, 

 //    0x816
input [31:0] count_config_op_48, 

 //    0x820
output [31:0] monitor_config_op_48, 

 //    0x824
input [31:0] count_config_op_49, 

 //    0x828
output [31:0] monitor_config_op_49, 

 //    0x832
input [31:0] count_config_op_50, 

 //    0x836
output [31:0] monitor_config_op_50, 

 //    0x840
input [31:0] count_config_op_51, 

 //    0x844
output [31:0] monitor_config_op_51, 

 //    0x848
input [31:0] count_config_op_52, 

 //    0x852
output [31:0] monitor_config_op_52, 

 //    0x856
input [31:0] count_config_op_53, 

 //    0x860
output [31:0] monitor_config_op_53, 

 //    0x864
input [31:0] count_config_op_54, 

 //    0x868
output [31:0] monitor_config_op_54, 

 //    0x872
input [31:0] count_config_op_55, 

 //    0x876
output [31:0] monitor_config_op_55, 

 //    0x880
input [31:0] count_config_op_56, 

 //    0x884
output [31:0] monitor_config_op_56, 

 //    0x888
input [31:0] count_config_op_57, 

 //    0x892
output [31:0] monitor_config_op_57, 

 //    0x896
input [31:0] count_config_op_58, 

 //    0x900
output [31:0] monitor_config_op_58, 

 //    0x904
input [31:0] count_config_op_59, 

 //    0x908
output [31:0] monitor_config_op_59, 

 //    0x912
input [31:0] count_config_op_60, 

 //    0x916
output [31:0] monitor_config_op_60, 

 //    0x920
input [31:0] count_config_op_61, 

 //    0x924
output [31:0] monitor_config_op_61, 

 //    0x928
input [31:0] count_config_op_62, 

 //    0x932
output [31:0] monitor_config_op_62, 

 //    0x936
input [31:0] count_config_op_63, 

 //    0x940
output [31:0] monitor_config_op_63
		);

wire [5:0] d_1_1, d_1_2, d_2_1, d_2_2, d_3_1, d_3_2, d_4_1, d_4_2, d_5_1, d_5_2, d_6_1, d_6_2, d_7_1, d_7_2, d_8_1, d_8_2, d_9_1, d_9_2, d_10_1, d_10_2, d_11_1, d_11_2, d_12_1, d_12_2, d_13_1, d_13_2, d_14_1, d_14_2, d_15_1, d_15_2, d_16_1, d_16_2, d_17_1, d_17_2, d_18_1, d_18_2, d_19_1, d_19_2, d_20_1, d_20_2, d_21_1, d_21_2, d_22_1, d_22_2, d_23_1, d_23_2, d_24_1, d_24_2, d_25_1, d_25_2, d_26_1, d_26_2, d_27_1, d_27_2, d_28_1, d_28_2, d_29_1, d_29_2, d_30_1, d_30_2, d_31_1, d_31_2, d_32_1, d_32_2;

wire [5:0] d_dac_1, d_dac_2, d_dac_3, d_dac_4, d_dac_5, d_dac_6, d_dac_7, d_dac_8, d_dac_9, d_dac_10, d_dac_11, d_dac_12, d_dac_13, d_dac_14, d_dac_15, d_dac_16, d_dac_17, d_dac_18, d_dac_19, d_dac_20, d_dac_21, d_dac_22, d_dac_23, d_dac_24, d_dac_25, d_dac_26, d_dac_27, d_dac_28, d_dac_29, d_dac_30, d_dac_31, d_dac_32, d_dac_33, d_dac_34, d_dac_35, d_dac_36;

wire [1:32] d_out_1, d_out_2;

wire [35:0] d_out_ip_dac;

assign monitor_config_in_0 [5:0] = d_dac_1 [5:0]; 
assign monitor_config_in_1 [5:0] = d_dac_2 [5:0]; 
assign monitor_config_in_2 [5:0] = d_dac_3 [5:0]; 
assign monitor_config_in_3 [5:0] = d_dac_4 [5:0]; 
assign monitor_config_in_4 [5:0] = d_dac_5 [5:0]; 
assign monitor_config_in_5 [5:0] = d_dac_6 [5:0]; 
assign monitor_config_in_6 [5:0] = d_dac_7 [5:0]; 
assign monitor_config_in_7 [5:0] = d_dac_8 [5:0]; 
assign monitor_config_in_8 [5:0] = d_dac_9 [5:0]; 
assign monitor_config_in_9 [5:0] = d_dac_10 [5:0]; 
assign monitor_config_in_10 [5:0] = d_dac_11 [5:0]; 
assign monitor_config_in_11 [5:0] = d_dac_12 [5:0]; 
assign monitor_config_in_12 [5:0] = d_dac_13 [5:0]; 
assign monitor_config_in_13 [5:0] = d_dac_14 [5:0]; 
assign monitor_config_in_14 [5:0] = d_dac_15 [5:0]; 
assign monitor_config_in_15 [5:0] = d_dac_16 [5:0]; 
assign monitor_config_in_16 [5:0] = d_dac_17 [5:0]; 
assign monitor_config_in_17 [5:0] = d_dac_18 [5:0]; 
assign monitor_config_in_18 [5:0] = d_dac_19 [5:0]; 
assign monitor_config_in_19 [5:0] = d_dac_20 [5:0]; 
assign monitor_config_in_20 [5:0] = d_dac_21 [5:0]; 
assign monitor_config_in_21 [5:0] = d_dac_22 [5:0]; 
assign monitor_config_in_22 [5:0] = d_dac_23 [5:0]; 
assign monitor_config_in_23 [5:0] = d_dac_24 [5:0]; 
assign monitor_config_in_24 [5:0] = d_dac_25 [5:0]; 
assign monitor_config_in_25 [5:0] = d_dac_26 [5:0]; 
assign monitor_config_in_26 [5:0] = d_dac_27 [5:0]; 
assign monitor_config_in_27 [5:0] = d_dac_28 [5:0]; 
assign monitor_config_in_28 [5:0] = d_dac_29 [5:0]; 
assign monitor_config_in_29 [5:0] = d_dac_30 [5:0]; 
assign monitor_config_in_30 [5:0] = d_dac_31 [5:0]; 
assign monitor_config_in_31 [5:0] = d_dac_32 [5:0]; 
assign monitor_config_in_32 [5:0] = d_dac_33 [5:0]; 
assign monitor_config_in_33 [5:0] = d_dac_34 [5:0]; 
assign monitor_config_in_34 [5:0] = d_dac_35 [5:0]; 
assign monitor_config_in_35 [5:0] = d_dac_36 [5:0]; 

assign monitor_config_op_0 [5:0] = d_1_1 [5:0];
assign monitor_config_op_1 [5:0] = d_1_2 [5:0];
assign monitor_config_op_2 [5:0] = d_2_1 [5:0];
assign monitor_config_op_3 [5:0] = d_2_2 [5:0];
assign monitor_config_op_4 [5:0] = d_3_1 [5:0];
assign monitor_config_op_5 [5:0] = d_3_2 [5:0];
assign monitor_config_op_6 [5:0] = d_4_1 [5:0];
assign monitor_config_op_7 [5:0] = d_4_2 [5:0];
assign monitor_config_op_8 [5:0] = d_5_1 [5:0];
assign monitor_config_op_9 [5:0] = d_5_2 [5:0];
assign monitor_config_op_10 [5:0] = d_6_1 [5:0];
assign monitor_config_op_11 [5:0] = d_6_2 [5:0];
assign monitor_config_op_12 [5:0] = d_7_1 [5:0];
assign monitor_config_op_13 [5:0] = d_7_2 [5:0];
assign monitor_config_op_14 [5:0] = d_8_1 [5:0];
assign monitor_config_op_15 [5:0] = d_8_2 [5:0];
assign monitor_config_op_16 [5:0] = d_9_1 [5:0];
assign monitor_config_op_17 [5:0] = d_9_2 [5:0];
assign monitor_config_op_18 [5:0] = d_10_1 [5:0];
assign monitor_config_op_19 [5:0] = d_10_2 [5:0];
assign monitor_config_op_20 [5:0] = d_11_1 [5:0];
assign monitor_config_op_21 [5:0] = d_11_2 [5:0];
assign monitor_config_op_22 [5:0] = d_12_1 [5:0];
assign monitor_config_op_23 [5:0] = d_12_2 [5:0];
assign monitor_config_op_24 [5:0] = d_13_1 [5:0];
assign monitor_config_op_25 [5:0] = d_13_2 [5:0];
assign monitor_config_op_26 [5:0] = d_14_1 [5:0];
assign monitor_config_op_27 [5:0] = d_14_2 [5:0];
assign monitor_config_op_28 [5:0] = d_15_1 [5:0];
assign monitor_config_op_29 [5:0] = d_15_2 [5:0];
assign monitor_config_op_30 [5:0] = d_16_1 [5:0];
assign monitor_config_op_31 [5:0] = d_16_2 [5:0];
assign monitor_config_op_32 [5:0] = d_17_1 [5:0];
assign monitor_config_op_33 [5:0] = d_17_2 [5:0];
assign monitor_config_op_34 [5:0] = d_18_1 [5:0];
assign monitor_config_op_35 [5:0] = d_18_2 [5:0];
assign monitor_config_op_36 [5:0] = d_19_1 [5:0];
assign monitor_config_op_37 [5:0] = d_19_2 [5:0];
assign monitor_config_op_38 [5:0] = d_20_1 [5:0];
assign monitor_config_op_39 [5:0] = d_20_2 [5:0];
assign monitor_config_op_40 [5:0] = d_21_1 [5:0];
assign monitor_config_op_41 [5:0] = d_21_2 [5:0];
assign monitor_config_op_42 [5:0] = d_22_1 [5:0];
assign monitor_config_op_43 [5:0] = d_22_2 [5:0];
assign monitor_config_op_44 [5:0] = d_23_1 [5:0];
assign monitor_config_op_45 [5:0] = d_23_2 [5:0];
assign monitor_config_op_46 [5:0] = d_24_1 [5:0];
assign monitor_config_op_47 [5:0] = d_24_2 [5:0];
assign monitor_config_op_48 [5:0] = d_25_1 [5:0];
assign monitor_config_op_49 [5:0] = d_25_2 [5:0];
assign monitor_config_op_50 [5:0] = d_26_1 [5:0];
assign monitor_config_op_51 [5:0] = d_26_2 [5:0];
assign monitor_config_op_52 [5:0] = d_27_1 [5:0];
assign monitor_config_op_53 [5:0] = d_27_2 [5:0];
assign monitor_config_op_54 [5:0] = d_28_1 [5:0];
assign monitor_config_op_55 [5:0] = d_28_2 [5:0];
assign monitor_config_op_56 [5:0] = d_29_1 [5:0];
assign monitor_config_op_57 [5:0] = d_29_2 [5:0];
assign monitor_config_op_58 [5:0] = d_30_1 [5:0];
assign monitor_config_op_59 [5:0] = d_30_2 [5:0];
assign monitor_config_op_60 [5:0] = d_31_1 [5:0];
assign monitor_config_op_61 [5:0] = d_31_2 [5:0];
assign monitor_config_op_62 [5:0] = d_32_1 [5:0];
assign monitor_config_op_63 [5:0] = d_32_2 [5:0];

///////////////////////////////////////////////////////////////////////////////////////////
//////////                      OUTPUT STAGE COMPENSATION                        //////////
///////////////////////////////////////////////////////////////////////////////////////////
 comp_amp comp_amp_OUTPUT_1_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_0[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_0[5:0]), .count_out(d_1_1), .ana_in(d_out_1[1]) ); 

 comp_amp comp_amp_OUTPUT_1_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_0[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_0[5:0]), .count_out(d_1_2), .ana_in(d_out_2[1]) ); 

 comp_amp comp_amp_OUTPUT_2_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_1[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_1[5:0]), .count_out(d_2_1), .ana_in(d_out_1[2]) ); 

 comp_amp comp_amp_OUTPUT_2_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_1[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_1[5:0]), .count_out(d_2_2), .ana_in(d_out_2[2]) ); 

 comp_amp comp_amp_OUTPUT_3_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_2[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_2[5:0]), .count_out(d_3_1), .ana_in(d_out_1[3]) ); 

 comp_amp comp_amp_OUTPUT_3_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_2[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_2[5:0]), .count_out(d_3_2), .ana_in(d_out_2[3]) ); 

 comp_amp comp_amp_OUTPUT_4_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_3[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_3[5:0]), .count_out(d_4_1), .ana_in(d_out_1[4]) ); 

 comp_amp comp_amp_OUTPUT_4_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_3[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_3[5:0]), .count_out(d_4_2), .ana_in(d_out_2[4]) ); 

 comp_amp comp_amp_OUTPUT_5_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_4[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_4[5:0]), .count_out(d_5_1), .ana_in(d_out_1[5]) ); 

 comp_amp comp_amp_OUTPUT_5_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_4[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_4[5:0]), .count_out(d_5_2), .ana_in(d_out_2[5]) ); 

 comp_amp comp_amp_OUTPUT_6_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_5[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_5[5:0]), .count_out(d_6_1), .ana_in(d_out_1[6]) ); 

 comp_amp comp_amp_OUTPUT_6_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_5[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_5[5:0]), .count_out(d_6_2), .ana_in(d_out_2[6]) ); 

 comp_amp comp_amp_OUTPUT_7_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_6[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_6[5:0]), .count_out(d_7_1), .ana_in(d_out_1[7]) ); 

 comp_amp comp_amp_OUTPUT_7_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_6[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_6[5:0]), .count_out(d_7_2), .ana_in(d_out_2[7]) ); 

 comp_amp comp_amp_OUTPUT_8_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_7[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_7[5:0]), .count_out(d_8_1), .ana_in(d_out_1[8]) ); 

 comp_amp comp_amp_OUTPUT_8_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_7[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_7[5:0]), .count_out(d_8_2), .ana_in(d_out_2[8]) ); 

 comp_amp comp_amp_OUTPUT_9_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_8[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_8[5:0]), .count_out(d_9_1), .ana_in(d_out_1[9]) ); 

 comp_amp comp_amp_OUTPUT_9_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_8[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_8[5:0]), .count_out(d_9_2), .ana_in(d_out_2[9]) ); 

 comp_amp comp_amp_OUTPUT_10_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_9[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_9[5:0]), .count_out(d_10_1), .ana_in(d_out_1[10]) ); 

 comp_amp comp_amp_OUTPUT_10_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_9[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_9[5:0]), .count_out(d_10_2), .ana_in(d_out_2[10]) ); 

 comp_amp comp_amp_OUTPUT_11_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_10[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_10[5:0]), .count_out(d_11_1), .ana_in(d_out_1[11]) ); 

 comp_amp comp_amp_OUTPUT_11_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_10[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_10[5:0]), .count_out(d_11_2), .ana_in(d_out_2[11]) ); 

 comp_amp comp_amp_OUTPUT_12_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_11[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_11[5:0]), .count_out(d_12_1), .ana_in(d_out_1[12]) ); 

 comp_amp comp_amp_OUTPUT_12_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_11[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_11[5:0]), .count_out(d_12_2), .ana_in(d_out_2[12]) ); 

 comp_amp comp_amp_OUTPUT_13_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_12[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_12[5:0]), .count_out(d_13_1), .ana_in(d_out_1[13]) ); 

 comp_amp comp_amp_OUTPUT_13_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_12[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_12[5:0]), .count_out(d_13_2), .ana_in(d_out_2[13]) ); 

 comp_amp comp_amp_OUTPUT_14_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_13[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_13[5:0]), .count_out(d_14_1), .ana_in(d_out_1[14]) ); 

 comp_amp comp_amp_OUTPUT_14_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_13[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_13[5:0]), .count_out(d_14_2), .ana_in(d_out_2[14]) ); 

 comp_amp comp_amp_OUTPUT_15_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_14[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_14[5:0]), .count_out(d_15_1), .ana_in(d_out_1[15]) ); 

 comp_amp comp_amp_OUTPUT_15_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_14[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_14[5:0]), .count_out(d_15_2), .ana_in(d_out_2[15]) ); 

 comp_amp comp_amp_OUTPUT_16_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_15[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_15[5:0]), .count_out(d_16_1), .ana_in(d_out_1[16]) ); 

 comp_amp comp_amp_OUTPUT_16_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_15[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_15[5:0]), .count_out(d_16_2), .ana_in(d_out_2[16]) ); 

 comp_amp comp_amp_OUTPUT_17_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_16[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_16[5:0]), .count_out(d_17_1), .ana_in(d_out_1[17]) ); 

 comp_amp comp_amp_OUTPUT_17_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_16[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_16[5:0]), .count_out(d_17_2), .ana_in(d_out_2[17]) ); 

 comp_amp comp_amp_OUTPUT_18_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_17[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_17[5:0]), .count_out(d_18_1), .ana_in(d_out_1[18]) ); 

 comp_amp comp_amp_OUTPUT_18_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_17[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_17[5:0]), .count_out(d_18_2), .ana_in(d_out_2[18]) ); 

 comp_amp comp_amp_OUTPUT_19_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_18[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_18[5:0]), .count_out(d_19_1), .ana_in(d_out_1[19]) ); 

 comp_amp comp_amp_OUTPUT_19_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_18[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_18[5:0]), .count_out(d_19_2), .ana_in(d_out_2[19]) ); 

 comp_amp comp_amp_OUTPUT_20_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_19[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_19[5:0]), .count_out(d_20_1), .ana_in(d_out_1[20]) ); 

 comp_amp comp_amp_OUTPUT_20_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_19[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_19[5:0]), .count_out(d_20_2), .ana_in(d_out_2[20]) ); 

 comp_amp comp_amp_OUTPUT_21_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_20[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_20[5:0]), .count_out(d_21_1), .ana_in(d_out_1[21]) ); 

 comp_amp comp_amp_OUTPUT_21_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_20[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_20[5:0]), .count_out(d_21_2), .ana_in(d_out_2[21]) ); 

 comp_amp comp_amp_OUTPUT_22_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_21[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_21[5:0]), .count_out(d_22_1), .ana_in(d_out_1[22]) ); 

 comp_amp comp_amp_OUTPUT_22_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_21[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_21[5:0]), .count_out(d_22_2), .ana_in(d_out_2[22]) ); 

 comp_amp comp_amp_OUTPUT_23_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_22[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_22[5:0]), .count_out(d_23_1), .ana_in(d_out_1[23]) ); 

 comp_amp comp_amp_OUTPUT_23_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_22[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_22[5:0]), .count_out(d_23_2), .ana_in(d_out_2[23]) ); 

 comp_amp comp_amp_OUTPUT_24_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_23[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_23[5:0]), .count_out(d_24_1), .ana_in(d_out_1[24]) ); 

 comp_amp comp_amp_OUTPUT_24_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_23[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_23[5:0]), .count_out(d_24_2), .ana_in(d_out_2[24]) ); 

 comp_amp comp_amp_OUTPUT_25_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_24[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_24[5:0]), .count_out(d_25_1), .ana_in(d_out_1[25]) ); 

 comp_amp comp_amp_OUTPUT_25_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_24[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_24[5:0]), .count_out(d_25_2), .ana_in(d_out_2[25]) ); 

 comp_amp comp_amp_OUTPUT_26_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_25[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_25[5:0]), .count_out(d_26_1), .ana_in(d_out_1[26]) ); 

 comp_amp comp_amp_OUTPUT_26_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_25[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_25[5:0]), .count_out(d_26_2), .ana_in(d_out_2[26]) ); 

 comp_amp comp_amp_OUTPUT_27_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_26[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_26[5:0]), .count_out(d_27_1), .ana_in(d_out_1[27]) ); 

 comp_amp comp_amp_OUTPUT_27_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_26[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_26[5:0]), .count_out(d_27_2), .ana_in(d_out_2[27]) ); 

 comp_amp comp_amp_OUTPUT_28_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_27[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_27[5:0]), .count_out(d_28_1), .ana_in(d_out_1[28]) ); 
//
 comp_amp comp_amp_OUTPUT_28_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_27[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_27[5:0]), .count_out(d_28_2), .ana_in(d_out_2[28]) ); 

 comp_amp comp_amp_OUTPUT_29_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_28[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_28[5:0]), .count_out(d_29_1), .ana_in(d_out_1[29]) ); 

 comp_amp comp_amp_OUTPUT_29_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_28[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_28[5:0]), .count_out(d_29_2), .ana_in(d_out_2[29]) ); 

 comp_amp comp_amp_OUTPUT_30_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_29[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_29[5:0]), .count_out(d_30_1), .ana_in(d_out_1[30]) ); 

 comp_amp comp_amp_OUTPUT_30_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_29[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_29[5:0]), .count_out(d_30_2), .ana_in(d_out_2[30]) ); 

 comp_amp comp_amp_OUTPUT_31_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_30[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_30[5:0]), .count_out(d_31_1), .ana_in(d_out_1[31]) ); 

 comp_amp comp_amp_OUTPUT_31_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_30[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_30[5:0]), .count_out(d_31_2), .ana_in(d_out_2[31]) ); 

 comp_amp comp_amp_OUTPUT_32_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_31[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_31[5:0]), .count_out(d_32_1), .ana_in(d_out_1[32]) ); 

 comp_amp comp_amp_OUTPUT_32_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_op_31[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_op_31[5:0]), .count_out(d_32_2), .ana_in(d_out_2[32]) ); 




///////////////////////////////////////////////////////////////////////////////////////////
//////////                      INPUT STAGE COMPENSATION                        //////////
///////////////////////////////////////////////////////////////////////////////////////////
  comp_amp comp_amp_INPUT_1 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_0[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_0[5:0]), .count_out(d_dac_1), .ana_in(d_out_ip_dac[0]) ); 

 comp_amp comp_amp_INPUT_2 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_1[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_1[5:0]), .count_out(d_dac_2), .ana_in(d_out_ip_dac[1]) ); 

 comp_amp comp_amp_INPUT_3 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_2[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_2[5:0]), .count_out(d_dac_3), .ana_in(d_out_ip_dac[2]) ); 

 comp_amp comp_amp_INPUT_4 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_3[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_3[5:0]), .count_out(d_dac_4), .ana_in(d_out_ip_dac[3]) ); 

 comp_amp comp_amp_INPUT_5 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_4[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_4[5:0]), .count_out(d_dac_5), .ana_in(d_out_ip_dac[4]) ); 

 comp_amp comp_amp_INPUT_6 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_5[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_5[5:0]), .count_out(d_dac_6), .ana_in(d_out_ip_dac[5]) ); 

 comp_amp comp_amp_INPUT_7 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_6[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_6[5:0]), .count_out(d_dac_7), .ana_in(d_out_ip_dac[6]) ); 

 comp_amp comp_amp_INPUT_8 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_7[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_7[5:0]), .count_out(d_dac_8), .ana_in(d_out_ip_dac[7]) ); 

 comp_amp comp_amp_INPUT_9 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_8[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_8[5:0]), .count_out(d_dac_9), .ana_in(d_out_ip_dac[8]) ); 

 comp_amp comp_amp_INPUT_10 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_9[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_9[5:0]), .count_out(d_dac_10), .ana_in(d_out_ip_dac[9]) ); 

 comp_amp comp_amp_INPUT_11 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_10[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_10[5:0]), .count_out(d_dac_11), .ana_in(d_out_ip_dac[10]) ); 

 comp_amp comp_amp_INPUT_12 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_11[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_11[5:0]), .count_out(d_dac_12), .ana_in(d_out_ip_dac[11]) ); 

 comp_amp comp_amp_INPUT_13 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_12[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_12[5:0]), .count_out(d_dac_13), .ana_in(d_out_ip_dac[12]) ); 

 comp_amp comp_amp_INPUT_14 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_13[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_13[5:0]), .count_out(d_dac_14), .ana_in(d_out_ip_dac[13]) ); 

 comp_amp comp_amp_INPUT_15 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_14[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_14[5:0]), .count_out(d_dac_15), .ana_in(d_out_ip_dac[14]) ); 

 comp_amp comp_amp_INPUT_16 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_15[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_15[5:0]), .count_out(d_dac_16), .ana_in(d_out_ip_dac[15]) ); 

 comp_amp comp_amp_INPUT_17 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_16[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_16[5:0]), .count_out(d_dac_17), .ana_in(d_out_ip_dac[16]) ); 

 comp_amp comp_amp_INPUT_18 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_17[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_17[5:0]), .count_out(d_dac_18), .ana_in(d_out_ip_dac[17]) ); 

 comp_amp comp_amp_INPUT_19 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_18[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_18[5:0]), .count_out(d_dac_19), .ana_in(d_out_ip_dac[18]) ); 

 comp_amp comp_amp_INPUT_20 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_19[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_19[5:0]), .count_out(d_dac_20), .ana_in(d_out_ip_dac[19]) ); 

 comp_amp comp_amp_INPUT_21 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_20[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_20[5:0]), .count_out(d_dac_21), .ana_in(d_out_ip_dac[20]) ); 

 comp_amp comp_amp_INPUT_22 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_21[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_21[5:0]), .count_out(d_dac_22), .ana_in(d_out_ip_dac[21]) ); 

 comp_amp comp_amp_INPUT_23 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_22[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_22[5:0]), .count_out(d_dac_23), .ana_in(d_out_ip_dac[22]) ); 

 comp_amp comp_amp_INPUT_24 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_23[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_23[5:0]), .count_out(d_dac_24), .ana_in(d_out_ip_dac[23]) ); 

 comp_amp comp_amp_INPUT_25 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_24[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_24[5:0]), .count_out(d_dac_25), .ana_in(d_out_ip_dac[24]) ); 

 comp_amp comp_amp_INPUT_26 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_25[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_25[5:0]), .count_out(d_dac_26), .ana_in(d_out_ip_dac[25]) ); 

 comp_amp comp_amp_INPUT_27 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_26[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_26[5:0]), .count_out(d_dac_27), .ana_in(d_out_ip_dac[26]) ); 

 comp_amp comp_amp_INPUT_28 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_27[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_27[5:0]), .count_out(d_dac_28), .ana_in(d_out_ip_dac[27]) ); 

 comp_amp comp_amp_INPUT_29 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_28[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_28[5:0]), .count_out(d_dac_29), .ana_in(d_out_ip_dac[28]) ); 

 comp_amp comp_amp_INPUT_30 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_29[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_29[5:0]), .count_out(d_dac_30), .ana_in(d_out_ip_dac[29]) ); 

 comp_amp comp_amp_INPUT_31 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_30[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_30[5:0]), .count_out(d_dac_31), .ana_in(d_out_ip_dac[30]) ); 

 comp_amp comp_amp_INPUT_32 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_31[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_31[5:0]), .count_out(d_dac_32), .ana_in(d_out_ip_dac[31]) ); 

 comp_amp comp_amp_INPUT_33 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_32[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_32[5:0]), .count_out(d_dac_33), .ana_in(d_out_ip_dac[32]) ); 

 comp_amp comp_amp_INPUT_34 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_33[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_33[5:0]), .count_out(d_dac_34), .ana_in(d_out_ip_dac[33]) ); 

 comp_amp comp_amp_INPUT_35 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_34[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_34[5:0]), .count_out(d_dac_35), .ana_in(d_out_ip_dac[34]) ); 

 comp_amp comp_amp_INPUT_36 (.clk(mclk), .reset(vmm_reset_control[6]), .rdb_wr(vmm_reset_control[18]), .local_wr(count_config_in_35[6]), .en(vmm_reset_control[19]), .lsb_tune(vmm_reset_control[17]), .config_count(count_config_in_35[5:0]), .count_out(d_dac_36), .ana_in(d_out_ip_dac[35]) ); 


///////////////////////////////////////////////////////////////////////////////////////////
//////////                      VMM TOP-MODULE                                   //////////
///////////////////////////////////////////////////////////////////////////////////////////
vmm_top vmm_top (
     .acore_clk(core_clk),
     .adc_0(adc_0[5:0]),
     .adc_1(adc_1[5:0]),
     .adc_2(adc_2[5:0]),
     .adc_3(adc_3[5:0]),
     .adc_4(adc_4[5:0]),
     .adc_5(adc_5[5:0]),
     .adc_6(adc_6[5:0]),
     .adc_7(adc_7[5:0]),
     .adc_8(adc_8[5:0]),
     .adc_9(adc_9[5:0]),
     .adc_10(adc_10[5:0]),
     .adc_11(adc_11[5:0]),
     .adc_12(adc_12[5:0]),
     .adc_13(adc_13[5:0]),
     .adc_14(adc_14[5:0]),
     .adc_15(adc_15[5:0]),
     .adc_16(adc_16[5:0]),
     .adc_17(adc_17[5:0]),
     .adc_18(adc_18[5:0]),
     .adc_19(adc_19[5:0]),
     .adc_20(adc_20[5:0]),
     .adc_21(adc_21[5:0]),
     .adc_22(adc_22[5:0]),
     .adc_23(adc_23[5:0]),
     .adc_24(adc_24[5:0]),
     .adc_25(adc_25[5:0]),
     .adc_26(adc_26[5:0]),
     .adc_27(adc_27[5:0]),
     .adc_28(adc_28[5:0]),
     .adc_29(adc_29[5:0]),
     .adc_30(adc_30[5:0]),
     .adc_31(adc_31[5:0]),
     .adc_clk(adc_clk),
     .adc_data_ready(vmm_ready_sram_dout[8]),
     .adc_mux_config(vmm_reset_control[28:24]),
     .adc_mux_wr(vmm_reset_control[0]),
     .adc_ref_h(Vref1_ADC),
     .adc_ref_l(Vref2_ADC),
     .addr(sram_din_control_addr[11:0]),
     .d_1_1(d_1_1), 
     .d_1_2(d_1_2), 
     .d_2_1(d_2_1), 
     .d_2_2(d_2_2), 
     .d_3_1(d_3_1), 
     .d_3_2(d_3_2), 
     .d_4_1(d_4_1), 
     .d_4_2(d_4_2), 
     .d_5_1(d_5_1), 
     .d_5_2(d_5_2), 
     .d_6_1(d_6_1), 
     .d_6_2(d_6_2), 
     .d_7_1(d_7_1), 
     .d_7_2(d_7_2), 
     .d_8_1(d_8_1), 
     .d_8_2(d_8_2), 
     .d_9_1(d_9_1), 
     .d_9_2(d_9_2), 
     .d_10_1(d_10_1), 
     .d_10_2(d_10_2), 
     .d_11_1(d_11_1), 
     .d_11_2(d_11_2), 
     .d_12_1(d_12_1), 
     .d_12_2(d_12_2), 
     .d_13_1(d_13_1), 
     .d_13_2(d_13_2), 
     .d_14_1(d_14_1), 
     .d_14_2(d_14_2), 
     .d_15_1(d_15_1), 
     .d_15_2(d_15_2), 
     .d_16_1(d_16_1), 
     .d_16_2(d_16_2), 
     .d_17_1(d_17_1), 
     .d_17_2(d_17_2), 
     .d_18_1(d_18_1), 
     .d_18_2(d_18_2), 
     .d_19_1(d_19_1), 
     .d_19_2(d_19_2), 
     .d_20_1(d_20_1), 
     .d_20_2(d_20_2), 
     .d_21_1(d_21_1), 
     .d_21_2(d_21_2), 
     .d_22_1(d_22_1), 
     .d_22_2(d_22_2), 
     .d_23_1(d_23_1), 
     .d_23_2(d_23_2), 
     .d_24_1(d_24_1), 
     .d_24_2(d_24_2), 
     .d_25_1(d_25_1), 
     .d_25_2(d_25_2), 
     .d_26_1(d_26_1), 
     .d_26_2(d_26_2), 
     .d_27_1(d_27_1), 
     .d_27_2(d_27_2), 
     .d_28_1(d_28_1), 
     .d_28_2(d_28_2), 
     .d_29_1(d_29_1), 
     .d_29_2(d_29_2), 
     .d_30_1(d_30_1), 
     .d_30_2(d_30_2), 
     .d_31_1(d_31_1), 
     .d_31_2(d_31_2), 
     .d_32_1(d_32_1), 
     .d_32_2(d_32_2), 
     .d_dac_1(d_dac_1), 
     .d_dac_2(d_dac_2), 
     .d_dac_3(d_dac_3), 
     .d_dac_4(d_dac_4), 
     .d_dac_5(d_dac_5), 
     .d_dac_6(d_dac_6), 
     .d_dac_7(d_dac_7), 
     .d_dac_8(d_dac_8), 
     .d_dac_9(d_dac_9), 
     .d_dac_10(d_dac_10), 
     .d_dac_11(d_dac_11), 
     .d_dac_12(d_dac_12), 
     .d_dac_13(d_dac_13), 
     .d_dac_14(d_dac_14), 
     .d_dac_15(d_dac_15), 
     .d_dac_16(d_dac_16), 
     .d_dac_17(d_dac_17), 
     .d_dac_18(d_dac_18), 
     .d_dac_19(d_dac_19), 
     .d_dac_20(d_dac_20), 
     .d_dac_21(d_dac_21), 
     .d_dac_22(d_dac_22), 
     .d_dac_23(d_dac_23), 
     .d_dac_24(d_dac_24), 
     .d_dac_25(d_dac_25), 
     .d_dac_26(d_dac_26), 
     .d_dac_27(d_dac_27), 
     .d_dac_28(d_dac_28), 
     .d_dac_29(d_dac_29), 
     .d_dac_30(d_dac_30), 
     .d_dac_31(d_dac_31), 
     .d_dac_32(d_dac_32), 
     .d_dac_33(d_dac_33), 
     .d_dac_34(d_dac_34), 
     .d_dac_35(d_dac_35), 
     .d_dac_36(d_dac_36), 
     .d_in(sram_din_control_addr[23:16]),
     .d_out_1(d_out_1[1:32]),
     .d_out_2(d_out_2[1:32]),
     .d_out_ip_dac(d_out_ip_dac[35:0]),
     .data_op(vmm_ready_sram_dout[7:0]),
     .ibias_m(Ibias_12uA),
     .m_clk(mclk),
     .r3_tune(vmm_reset_control[16:8]),
     .r_tune(vmm_reset_control[20]),
     .reset_adc(vmm_reset_control[3]),
     .sh_en(vmm_reset_control[2]),
     .sram_rd(sram_din_control_addr[13]),
     .sram_reset(vmm_reset_control[4]),
     .sram_wr(sram_din_control_addr[14]),
     .v_ref_comp_h(Vref_h_comp),
     .v_ref_comp_l(Vref_l_comp),
     .v_ref_ip_dac_h(Vref_h_in),
     .v_ref_ip_dac_l(Vref_l_in),
     .vcm(VCM_0p4V),
     .vcm_en(vmm_reset_control[2])
       );

endmodule
