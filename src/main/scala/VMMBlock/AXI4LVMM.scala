// SPDX-License-Identifier: Apache-2.0

// Chisel module for wrapping VMM Accelerator to AXI4-Lite
// Inititally written by Aleksi Korsman, aleksi.korsman@aalto.fi, 2022-10-27
package vmmblock

import chisel3._
import chisel3.util._
import chisel3.experimental._
import amba.common._
import amba.axi4l._
import scala.math._
import chisel3.stage.{ChiselStage, ChiselGeneratorAnnotation}

trait ControlIO {
  val vmm_reset_control     = Input  (UInt(32.W))
  val sram_din_control_addr = Input  (UInt(32.W))
  val vmm_ready_sram_dout   = Output (UInt(32.W))
  val adc                   = Output (Vec(32, UInt(32.W))) 
  val count_config_in       = Input  (Vec(36, UInt(32.W)))
  val monitor_config_in     = Output (Vec(36, UInt(32.W)))
  val count_config_op       = Input  (Vec(64, UInt(32.W)))
  val monitor_config_op     = Output (Vec(64, UInt(32.W)))
}

trait AnalogIO {
  val mclk = Input(UInt(1.W))
  val VCM_0p4V = Input(UInt(1.W))
  val adc_clk = Input(UInt(1.W))
  val Vref1_ADC = Input(UInt(1.W))
  val Vref2_ADC = Input(UInt(1.W))
  val Ibias_12uA = Input(UInt(1.W))
  val Vref_l_comp = Input(UInt(1.W))
  val Vref_h_comp = Input(UInt(1.W))
  val Vref_l_in = Input(UInt(1.W))
  val Vref_h_in = Input(UInt(1.W))
}

// IO types for AXI4LVMM
class VMMBlockAnalogIO extends Bundle with AnalogIO
class VMMBlockControlIO extends Bundle with ControlIO
class VMMBlockFlatIO extends Bundle with AnalogIO with ControlIO {
  val core_clk = Input(Clock())
}

/** GPIO module with AXI4-Lite interface.
  * @param addr_width
  *   Address bus width
  * @param data_widht
  *   Data bus width
  * @param XLEN
  *   Global register width
  * @param sim
  *   Simulation flag, uses a dummy model. Default false
  */
class AXI4LVMM(addr_width: Int, data_width: Int, XLEN: Int = 32, sim: Boolean = false) extends RawModule
    with AXI4LSlave {

  // AXI4-lite interface
  val clk_rst    = IO(new AXI4LClkRst)
  val slave_port = IO(new AXI4LIO(addr_width = addr_width, data_width = data_width))

  // Inputs and outputs
  val io = IO(new VMMBlockAnalogIO)

  val bytes = data_width / 8
  
  withClockAndReset(clk_rst.ACLK, !clk_rst.ARESETn) {

    // Module to be wrapped
    val vmm = {if (!sim) Module(new control_wrapper) else Module(new DummyVMM)}

    // Memory map for input and output registers
    val memory_map: Map[Data, MemoryRange] = (
      Seq(vmm.io.vmm_reset_control     -> MemoryRange(begin = 0, end = 3, mode = MemoryMode.RW, init = 0x110000)) ++
      Seq(vmm.io.sram_din_control_addr -> MemoryRange(begin = 4, end = 7, mode = MemoryMode.RW)) ++
      Seq(vmm.io.vmm_ready_sram_dout   -> MemoryRange(begin = 8, end = 11, mode = MemoryMode.R)) ++
      (0 until 32).map(i => vmm.io.adc(i)              -> MemoryRange(begin = 12  + i*4, end = 15  + i*4, mode = MemoryMode.R)) ++
      (0 until 36).map(i => vmm.io.count_config_in(i)  -> MemoryRange(begin = 140 + i*8, end = 143 + i*8, mode = MemoryMode.RW)) ++
      (0 until 36).map(i => vmm.io.monitor_config_in(i)-> MemoryRange(begin = 144 + i*8, end = 147 + i*8, mode = MemoryMode.R)) ++
      (0 until 64).map(i => vmm.io.count_config_op(i)  -> MemoryRange(begin = 428 + i*8, end = 431 + i*8, mode = MemoryMode.RW)) ++
      (0 until 64).map(i => vmm.io.monitor_config_op(i)-> MemoryRange(begin = 432 + i*8, end = 435 + i*8, mode = MemoryMode.R))
    ).toMap

    // IOs to be passed through (not AXI controlled)
     vmm.io.mclk       := io.mclk                        
     vmm.io.VCM_0p4V   := io.VCM_0p4V                    
     vmm.io.adc_clk    := io.adc_clk                     
     vmm.io.Vref1_ADC  := io.Vref1_ADC                   
     vmm.io.Vref2_ADC  := io.Vref2_ADC                   
     vmm.io.Ibias_12uA := io.Ibias_12uA                  
     vmm.io.Vref_l_comp:= io.Vref_l_comp                 
     vmm.io.Vref_h_comp:= io.Vref_h_comp                 
     vmm.io.Vref_l_in  := io.Vref_l_in                   
     vmm.io.Vref_h_in  := io.Vref_h_in                   

    // Module clock
    vmm.io.core_clk := clk_rst.ACLK

    // Registers
    val axi_regs = RegInit(VecInit.tabulate(memory_map(vmm.io.monitor_config_op(63)).begin.toInt/4 + 1) {
      idx => memory_map.find(_._2.begin == idx*4).getOrElse(None -> MemoryRange(1,2,MemoryMode.RW))._2.init.U(32.W)
    })
    val read_buf = RegInit(0.U(XLEN.W)) // Read buffer
    
    // Iterate over all entries in the memory map and connect IOs
    for ((data, memory_range) <- memory_map) {
      if (memory_range.mode == MemoryMode.R) {
        axi_regs(memory_range.begin.toInt/4) := data
      }
      else if (memory_range.mode == MemoryMode.RW) {
        data := axi_regs(memory_range.begin.toInt/4)
      }
    }

    // SRAM to AXI interface
    val sram_iface = Module(new AXI4LtoSRAM(addr_width, data_width))
    sram_iface.clk_rst    <> clk_rst
    sram_iface.slave_port <> slave_port

    val addr_offset = (math.log(bytes) / math.log(2)).round // log2(bytes)
    val raddr       = Cat(sram_iface.io.raddr(addr_width - 1, addr_offset), 0.U(2.W))
    val waddr       = Cat(sram_iface.io.waddr(addr_width - 1, addr_offset), 0.U(2.W))

    // read port
    sram_iface.io.rdata := read_buf
    when(sram_iface.io.ren && (raddr <= memory_map(vmm.io.monitor_config_op(63)).end.asUInt)) {
      read_buf := axi_regs(raddr >> 2)
    } .otherwise {
      read_buf := 0.U
    }

    // write port
    when(sram_iface.io.wen && (waddr <= memory_map(vmm.io.monitor_config_op(63)).end.asUInt)) {
      val new_out = Wire(Vec(bytes, UInt(8.W)))
      // Edit only the bytes defined by wmask
      for (i <- 0 until bytes) {
        when(sram_iface.io.wmask(i) === 1.U) {
          new_out(i) := sram_iface.io.wdata((i + 1) * 8 - 1, i * 8)
        }.otherwise {
          new_out(i) := axi_regs(waddr >> 2)((i + 1) * 8 - 1, i * 8)
        }
      }
      for ((data, memory_range) <- memory_map) {
        if (memory_range.mode == MemoryMode.RW) {
          when ((waddr >> 2) === (memory_range.begin/4).U) {
            axi_regs(memory_range.begin.toInt/4) := new_out.reduce((a, b) => Cat(b, a))
          }
        }
      }
    }
  }
}

/** Test wrapper for AXI4LVMM. Renames AXI4L signals to standard format.
  * @param addr_width
  *   Address bus width
  * @param data_width
  *   Data bus width
  * @param XLEN
  *   Global register width
  */
class AXI4LVMMTestWrapper(addr_width: Int, data_width: Int, XLEN: Int = 32) extends RawModule {
  val axi4lvmm = Module(new AXI4LVMM(addr_width, data_width, XLEN, sim = true))

  val io = IO(new VMMBlockAnalogIO)

  io <> axi4lvmm.io

  val axi_io = IO(new Bundle {
    // Clock and Reset
    val aclk    = Input(Clock())
    val aresetn = Input(Bool())
    // Read Address Channel
    val arready = Output(Bool())
    val arvalid = Input(Bool())
    val araddr  = Input(UInt(addr_width.W))
    val arprot  = Input(UInt(3.W))
    // Read Data Channel
    val rready = Input(Bool())
    val rvalid = Output(Bool())
    val rdata  = Output(UInt(data_width.W))
    val rresp  = Output(Response())
    // Write Address Channel
    val awready = Output(Bool())
    val awvalid = Input(Bool())
    val awaddr  = Input(UInt(addr_width.W))
    val awprot  = Input(UInt(3.W))
    // Write Data Channel
    val wready = Output(Bool())
    val wvalid = Input(Bool())
    val wdata  = Input(UInt(data_width.W))
    val wstrb  = Input(UInt((data_width / 8).W))
    // Write Response Channel
    val bready = Input(Bool())
    val bvalid = Output(Bool())
    val bresp  = Output(Response())
  })

  axi_io.arready := axi4lvmm.slave_port.AR.ready
  axi_io.rvalid  := axi4lvmm.slave_port.R.valid
  axi_io.rdata   := axi4lvmm.slave_port.R.bits.DATA
  axi_io.rresp   := axi4lvmm.slave_port.R.bits.RESP
  axi_io.awready := axi4lvmm.slave_port.AW.ready
  axi_io.wready  := axi4lvmm.slave_port.W.ready
  axi_io.bvalid  := axi4lvmm.slave_port.B.valid
  axi_io.bresp   := axi4lvmm.slave_port.B.bits.RESP

  axi4lvmm.clk_rst.ACLK            := axi_io.aclk
  axi4lvmm.clk_rst.ARESETn         := axi_io.aresetn
  axi4lvmm.slave_port.AR.valid     := axi_io.arvalid
  axi4lvmm.slave_port.AR.bits.ADDR := axi_io.araddr
  axi4lvmm.slave_port.AR.bits.PROT := axi_io.arprot
  axi4lvmm.slave_port.R.ready      := axi_io.rready
  axi4lvmm.slave_port.AW.valid     := axi_io.awvalid
  axi4lvmm.slave_port.AW.bits.ADDR := axi_io.awaddr
  axi4lvmm.slave_port.AW.bits.PROT := axi_io.awprot
  axi4lvmm.slave_port.W.valid      := axi_io.wvalid
  axi4lvmm.slave_port.W.bits.DATA  := axi_io.wdata
  axi4lvmm.slave_port.W.bits.STRB  := axi_io.wstrb
  axi4lvmm.slave_port.B.ready      := axi_io.bready
}

// Ensures for compiler that both VMM and DummyVMM have IO named io
trait VMMIO {
  def io: VMMBlockFlatIO
}

// Class for Verilog blackbox
class control_wrapper extends BlackBox with HasBlackBoxResource with VMMIO {
  val io = IO(new VMMBlockFlatIO)
  addResource("/verilog/comp_amp.v")
  addResource("/verilog/control_wrapper.v")
  addResource("/verilog/vmm_top.v")
}

// Dummy VMM for simulations
class DummyVMM extends Module with VMMIO {
  val io = IO(new VMMBlockFlatIO)
  io.vmm_ready_sram_dout := 0.U
  io.adc.foreach(_ := 0.U)
  io.monitor_config_in.getElements.foreach(_ := 0.U)
  io.monitor_config_op.getElements.foreach(_ := 0.U)
  io.adc.foreach(_ := ~io.vmm_reset_control)
  dontTouch(io)
}

object AXI4LVMM extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LVMM(addr_width = 32, data_width = 32)))
  (new ChiselStage).execute(args, annos)
}

object AXI4LVMMTestWrapper extends App {
  val annos = Seq(ChiselGeneratorAnnotation(() => new AXI4LVMMTestWrapper(addr_width = 32, data_width = 32)))
  (new ChiselStage).execute(args, annos)
}

