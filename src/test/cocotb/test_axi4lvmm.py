# SPDX-License-Identifier: Apache-2.0

# pip install cocotb
import cocotb
from cocotb.clock import Clock
from cocotb.triggers import ClockCycles, with_timeout
# pip install cocotbext-axi
from cocotbext.axi import AxiLiteMaster, AxiLiteBus
from numpy import byte
from enum import Enum

class MemoryMode(Enum):
    R  = 1
    RW = 2

# name : (address, access type, initial value)
memory_map = {"vmm_reset_control"     : (0, MemoryMode.RW, 0x110000),
              "sram_din_control_addr" : (4, MemoryMode.RW, 0),
              "vmm_ready_sram_dout"   : (8, MemoryMode.R, 0),
              "adc_0"                 : (12, MemoryMode.R, 0),
              "adc_31"                : (136, MemoryMode.R, 0),
              "count_config_in_0"     : (140, MemoryMode.RW, 0),
              "monitor_config_in_0"   : (144, MemoryMode.R, 0),
              "count_config_in_35"    : (420, MemoryMode.RW, 0),
              "monitor_config_in_35"  : (424, MemoryMode.R, 0),
              "count_config_op_0"     : (428, MemoryMode.RW, 0),
              "monitor_config_op_0"   : (432, MemoryMode.R, 0),
              "count_config_op_63"    : (932, MemoryMode.RW, 0),
              "monitor_config_op_63"  : (936, MemoryMode.R, 0)}

# Data as a hex string
async def test_write_and_read(axilite_master, addr, data):
    test_write_data = bytearray.fromhex(str(data))
    await with_timeout(axilite_master.init_write(addr, test_write_data).wait(), 50, timeout_unit='us')
    data = axilite_master.init_read(addr, length=4)
    await with_timeout(data.wait(), 50, timeout_unit='us')
    assert(data.data.data == test_write_data)

async def read_transaction(axilite_master, addr):
    data = axilite_master.init_read(addr, length=4)
    await with_timeout(data.wait(), 50, timeout_unit='us')
    return data.data.data


@cocotb.test()
async def test_axi4lvmm(dut):
    aclk = dut.axi_io_aclk
    arstn = dut.axi_io_aresetn
    arstn.value = 1
    axilite_master = AxiLiteMaster(AxiLiteBus.from_prefix(dut, "axi_io"), aclk, arstn, reset_active_level=False)
    cocotb.start_soon(Clock(aclk, 1, units="us").start())
    await ClockCycles(aclk, 2)
    arstn.value = 0
    await ClockCycles(aclk, 2)
    arstn.value = 1
    await ClockCycles(aclk, 2)

    dut._log.info("Check initial values")
    for key in memory_map:
        if memory_map[key][1] == MemoryMode.RW:
            data = await read_transaction(axilite_master, memory_map[key][0])
            io_data = getattr(dut, "axi4lvmm.vmm.io_" + str(key))
            assert(int.from_bytes(data, byteorder='little') == memory_map[key][2])
            assert(int(str(io_data.value), 2) == memory_map[key][2])

    dut._log.info("Test write to output register") 
    for key in memory_map:
        if memory_map[key][1] == MemoryMode.RW:
            test_data = "ABCDEF00"
            await test_write_and_read(axilite_master, memory_map[key][0], test_data)
            # Check that IOs changed value too
            io_data = getattr(dut,"axi4lvmm.vmm.io_" + str(key))
            assert(int(str(io_data.value), 2) == int.from_bytes(bytearray.fromhex(test_data), byteorder='little'))

    dut._log.info("Test read for inputs") 
    for key in memory_map:
        if memory_map[key][1] == MemoryMode.R:
            test_data = 0xCAFEBABA
            io = getattr(dut, "axi4lvmm.vmm.io_" + str(key))
            io.value = test_data
            data = await read_transaction(axilite_master, memory_map[key][0])
            assert(int.from_bytes(data, byteorder='little') == test_data)

